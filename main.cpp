#include <stdio.h>
#include <stdlib.h>

struct Definition
{
	double a, b, c;
};

struct State
{
	Definition def;
};

double calcValue(double x, Definition d, State* s)
{
	// Check for differences between current state and input definition
	if ((d.a != s->def.a) || (d.b != s->def.b) || (d.c != s->def.c))
	{
		// Store definition as new state
		s->def.a = d.a;
		s->def.b = d.b;
		s->def.c = d.c;
	}
	return d.c + (x * (d.b + x * d.a));
}

State* initState()
{
	State* s = (State*)malloc(sizeof(State));
	if (s == NULL) { return NULL; }
	s->def.a = 0;
	s->def.b = 0;
	s->def.c = 0;

	return s;
}

void freeState(State* s)
{
	free(s);
}

void printState(State* s)
{
	printf("f(X) = %f * X^2 + %f * X + %f\n", s->def.a, s->def.b, s->def.c);
}

void CalcAndReport(double x, Definition d, State* s)
{
	double res;
	printf("\n");
	res = calcValue(x, d, s);
	printState(s);
	printf("Result : f(%f) = %f\n", x, res);
}

int main(int argc, char* argv[])
{
	double x;
	Definition d, d2;
	State *s, *s2;

	s = initState();
	if (s == NULL) { return -1; }

	s2 = initState();
	if (s2 == NULL) { return -1; }

	printState(s);

	// First time through, s should be populated
	d.a = 1; d.b = 2; d.c = 3;
	x = 1;
	CalcAndReport(x, d, s);

	// Second time through, d did not change, so no change to s
	x = 2;
	CalcAndReport(x, d, s);

	// Third time, d has changed, so s should be different
	d.b = 1;
	CalcAndReport(x, d, s);

	freeState(s);
	freeState(s2);

	return 0;
}

